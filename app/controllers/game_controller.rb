# frozen_string_literal: true

class GameController < ApplicationController
  before_action :authenticate_user!

  def show
    @user = current_user
    @game_data = @user.to_json

    render json: @game_data if request.xhr? # Else render the view
  end
end
