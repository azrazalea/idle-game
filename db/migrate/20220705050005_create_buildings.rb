# frozen_string_literal: true

class CreateBuildings < ActiveRecord::Migration[7.0]
  def change
    create_table :buildings do |t|
      t.integer :woodcutter_hut, default: 0, null: false
      t.integer :quarry, default: 0, null: false
      t.integer :mine, default: 0, null: false
      t.integer :farm, default: 0, null: false
      t.integer :toolshed, default: 0, null: false
      t.references :user, null: false, foreign_key: true, index: { unique: true }

      t.timestamps
    end
  end
end
