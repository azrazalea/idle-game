# frozen_string_literal: true

require 'test_helper'

class BuildingTest < ActiveSupport::TestCase
  test 'cannot create duplicate building' do
    building = Building.new(user: User.first)
    assert_not building.valid?
    assert_equal(building.errors.to_a.length, 1)
    assert_equal(building.errors.first.type,  :taken)
  end
end
