# frozen_string_literal: true

class AddMetadataToUser < ActiveRecord::Migration[7.0]
  def change
    change_table :users, bulk: true do |t|
      t.string :display_name, limit: 25, null: false, default: 'I have no name'
      t.string :gender, limit: 16, null: false, default: 'Genderless'
      t.string :avatar, null: true
      t.string :job, limit: 16, null: true
    end
  end
end
