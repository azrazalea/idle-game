# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  JOB_LIST = %i[
    woodcutter
    stonecutter
    miner
    farmer
    builder
    recruiter
    trainer
  ].freeze
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  default_scope { eager_load(:resource, :villager, :building) }

  def work
    send("work_#{job}")
    villager_work = villager.work
    resource.wood += villager_work[:wood]
    resource.stone += villager_work[:stone]
    resource.gold += villager_work[:gold]
    resource.food += villager_work[:food]
    save!
    GameChannel.broadcast_to(self, **to_json)
  rescue NoMethodError
    logger.error("User #{id} cannot work #{job.inspect}")
  end

  def to_json(*_args)
    React.camelize_props({
                           id:,
                           display_name:,
                           gender:,
                           avatar:,
                           job:,
                           resources: {
                             wood: resource.wood,
                             stone: resource.stone,
                             gold: resource.gold,
                             food: resource.food
                           },
                           villagers: {
                             unemployed: villager.unemployed,
                             trainee: villager.trainee,
                             woodcutter: villager.woodcutter,
                             stonecutter: villager.stonecutter,
                             miner: villager.miner,
                             farmer: villager.farmer,
                             builder: villager.builder
                           },
                           buildings: {
                             woodcutter_hut: building.woodcutter_hut,
                             quarry: building.quarry,
                             mine: building.mine,
                             farm: building.farm,
                             toolshed: building.toolshed
                           }
                         })
  end

  private

  def work_woodcutter
    resource.wood += 10
  end

  def work_stonecutter
    resource.stone += 10
  end

  def work_miner
    resource.gold += 10
  end

  def work_farmer
    resource.food += 10
  end

  def work_recruiter
    if resource.food < 10 || resource.gold < 10
      logger.debug("Cannot recruit for #{id} at #{resource.food} food and #{resource.gold} gold")
      return
      # FIXME: Notify user of required resources
    end

    resource.food -= 10
    resource.gold -= 10
    villager.unemployed += 1
  end

  def work_trainer
    return if villager.unemployed < 1

    if job_argument.blank?
      logger.debug("Cannot train for #{id} nothing")
      return
    end

    villager.send("#{job_argument}=", villager.send(job_argument) + 1)
    villager.unemployed -= 1
  rescue NoMethodError
    logger.error("Not able to train villager to #{job_argument.inspect}")
  end

  def work_
    logger.warn("User #{id} is not assigned a job")
  end

  # These associations all have the same behavior currently and are basically
  # named datastores for User that need to always be created and unique to user.
  [Resource, Villager, Building].each do |type|
    type_name = type.to_s.downcase
    method_name = "ensure_#{type_name}".to_sym
    has_one type_name.to_sym, dependent: :destroy, autosave: true, required: true, inverse_of: :user
    before_validation method_name

    define_method(method_name) do
      send("#{type_name}=", type.new(user: self)) unless send(type_name)
    end
  end
end
