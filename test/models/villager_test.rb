# frozen_string_literal: true

require 'test_helper'

class VillagerTest < ActiveSupport::TestCase
  test 'cannot create duplicate villager' do
    villager = Villager.new(user: User.first)
    assert_not villager.valid?
    assert_equal(villager.errors.to_a.length, 1)
    assert_equal(villager.errors.first.type,  :taken)
  end
end
