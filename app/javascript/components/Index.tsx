import * as React from 'react'
import _ from 'lodash'
import { GameInterface } from '../interfaces/game_interfaces'
import GameWindow from './GameWindow'
import ActionCable from '@rails/actioncable'
import consumer from '../channels/consumer'

export default class IndexComponent extends React.Component<GameInterface, GameInterface> {
  channel: ActionCable.Channel

  updateUser = (key: string, value: string) => {
    const element: HTMLMetaElement | null = document?.querySelector("[name='csrf-token']")
    const csrfToken = element?.content

    fetch('/user', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRF-Token': csrfToken || ''
      },
      body: JSON.stringify({ user: { [key]: value } })
    }).then(response => response.json()).then(data => this.updateGameState(data)).catch(error => console.log(error))
  }

  constructor (props: GameInterface) {
    super(props)
    this.state = props
  }

  componentDidMount = () => {
    this.channel = consumer.subscriptions.create('GameChannel', {
      initialized () {
        console.log('initialized')
      },
      connected () {
        console.log('connected')
      },
      rejected () {
        console.log('rejected')
      },
      received: (data: GameInterface) => {
        this.updateGameState(data)
      }
    })
  }

  componentWillUnmount () {
    this.channel.unsubscribe()
  }

  render () {
    return (
      <GameWindow gameState={this.state} updateUser={_.debounce(this.updateUser, 200)} />
    )
  }

  updateGameState = (data: GameInterface) => {
    this.setState(_.merge({}, this.state, data))
  }
}
