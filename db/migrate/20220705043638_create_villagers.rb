# frozen_string_literal: true

class CreateVillagers < ActiveRecord::Migration[7.0]
  def change
    create_table :villagers do |t|
      t.integer :unemployed, default: 0, null: false
      t.integer :woodcutter, default: 0, null: false
      t.integer :stonecutter, default: 0, null: false
      t.integer :miner, default: 0, null: false
      t.integer :farmer, default: 0, null: false
      t.integer :builder, default: 0, null: false
      t.integer :trainee, default: 0, null: false
      t.references :user, null: false, foreign_key: true, index: { unique: true }

      t.timestamps
    end
  end
end
