# frozen_string_literal: true

class Resource < ApplicationRecord
  belongs_to :user
  validates :user, uniqueness: true
end
