# frozen_string_literal: true

require 'test_helper'

class GameChannelTest < ActionCable::Channel::TestCase
  test 'subscribes' do
    stub_connection current_user: User.first
    subscribe
    assert_has_stream_for User.first
  end
end
