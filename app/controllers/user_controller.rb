# frozen_string_literal: true

class UserController < ApplicationController
  before_action :authenticate_user!

  def show
    render json: current_user
  end

  def update
    @user = current_user
    @user.update(params.require(:user).permit(:display_name, :gender, :job, :job_argument))

    render json: @user.to_json
  end
end
