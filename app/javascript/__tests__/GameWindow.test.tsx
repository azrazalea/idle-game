import React from 'react'
import { render, screen } from '@testing-library/react'
import GameWindow from '../components/GameWindow'
import { GameInterface } from '../interfaces/game_interfaces'

const defaultProps: GameInterface = {
  id: 1,
  displayName: 'Test Name Please Ignore',
  gender: "Wouldn't you like to know",
  avatar: null,
  job: null,
  jobArgument: null,
  buildings: {
    woodcutterHut: 0,
    quarry: 0,
    mine: 0,
    farm: 0,
    toolshed: 0
  },
  resources: {
    wood: 0,
    stone: 0,
    gold: 0,
    food: 0
  },
  villagers: {
    unemployed: 0,
    trainee: 0,
    woodcutter: 0,
    stonecutter: 0,
    miner: 0,
    farmer: 0,
    builder: 0
  }
}

const onChange = (test: any, tester: any) => console.log(test)

describe('<GameWindow/>', () => {
  test('should display a GameWindow table', async () => {
    render(<GameWindow gameState={defaultProps} updateUser={onChange} />)
    const gameWindow = screen.getByTestId('game-window')

    expect(gameWindow).toHaveTextContent('Welcome, Test Name Please Ignore')
    expect(gameWindow).toHaveTextContent("Gender: Wouldn't you like to know Current Job:Nothing")
    expect(gameWindow).toHaveTextContent('Resources')
    expect(gameWindow).toHaveTextContent('Villagers')
    expect(gameWindow).toHaveTextContent('Buildings')
    expect(gameWindow).toHaveTextContent('Gold0')
    expect(gameWindow).toHaveTextContent('Miner0')
    expect(gameWindow).toHaveTextContent('Mine0')
    expect(gameWindow).toHaveTextContent('Food0')
    expect(gameWindow).toHaveTextContent('Farmer0')
    expect(gameWindow).toHaveTextContent('Farm0')
    expect(gameWindow).toHaveTextContent('Wood0')
    expect(gameWindow).toHaveTextContent('Woodcutter0')
    expect(gameWindow).toHaveTextContent('Woodcutter Hut0')
    expect(gameWindow).toHaveTextContent('Stone0')
    expect(gameWindow).toHaveTextContent('Stonecutter0')
    expect(gameWindow).toHaveTextContent('Quarry0')
  })

  test('should handle display correctly', async () => {
    const { rerender } = render(<GameWindow gameState={defaultProps} updateUser={onChange} />)
    const gameWindow = screen.getByTestId('game-window')
    const updatedName = { ...defaultProps, ...{ displayName: 'Rawr' } }

    expect(gameWindow).toHaveTextContent('Welcome, Test Name Please Ignore')
    rerender(<GameWindow gameState={updatedName} updateUser={onChange} />)
    expect(gameWindow).toHaveTextContent('Welcome, Rawr')
  })
})
