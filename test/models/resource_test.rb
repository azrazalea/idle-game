# frozen_string_literal: true

require 'test_helper'

class ResourceTest < ActiveSupport::TestCase
  test 'cannot create duplicate resource' do
    resource = Resource.new(user: User.first)
    assert_not resource.valid?
    assert_equal(resource.errors.to_a.length, 1)
    assert_equal(resource.errors.first.type,  :taken)
  end
end
