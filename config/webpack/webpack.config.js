const path = require('path')
const webpack = require('webpack')
const mode = process.env.NODE_ENV === 'development' ? 'development' : 'production'
const ESLintPlugin = require('eslint-webpack-plugin')
const mainApp = path.resolve(__dirname, '..', '..', 'app', 'javascript')

module.exports = {
  mode,
  optimization: {
    moduleIds: 'deterministic'
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: ['ts-loader']
      }
    ]
  },
  entry: {
    application: path.resolve(mainApp, 'application.ts')
  },
  output: {
    filename: '[name].js',
    sourceMapFilename: '[file].map',
    path: path.resolve(__dirname, '..', '..', 'app/assets/builds')
  },
  resolve: {
    extensions: ['.ts', '.tsx', '...']
  },
  plugins: [
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1
    }),
    new ESLintPlugin({
      context: mainApp,
      extensions: ['ts', 'tsx', 'jsx', 'js']
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      JQuery: 'jquery',
      Popper: ['popper.js', 'default'] // for Bootstrap 4
    })
  ]
}
