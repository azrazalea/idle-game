# frozen_string_literal: true

require 'test_helper'

module ApplicationCable
  class ConnectionTest < ActionCable::Connection::TestCase
    Warden = Struct.new(:user)
    Env = Struct.new(:warden)

    include Devise::Test::IntegrationHelpers

    setup do
      warden = Warden.new(User.first)
      connect env: { 'warden' => warden }
    end

    test 'connects through devise' do
      assert_equal connection.current_user, User.first
    end
  end
end
