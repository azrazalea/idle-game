# frozen_string_literal: true

class Villager < ApplicationRecord
  JOB_LIST = %i[
    woodcutter
    stonecutter
    miner
    farmer
    builder
    recruiter
    trainer
  ].freeze
  belongs_to :user
  validates :user, uniqueness: true

  def work
    {
      wood: woodcutter,
      stone: stonecutter,
      gold: miner,
      food: farmer,
      build: builder
    }
  end
end
