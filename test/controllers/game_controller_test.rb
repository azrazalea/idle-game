# frozen_string_literal: true

require 'test_helper'
require 'json'

class GameControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'show correctly renders GameWindow' do
    user = User.first
    sign_in user
    get root_url

    assert_equal 200, response.status

    assert_react_component 'Index' do |props|
      assert_equal user.id, props[:id]
      assert_equal user.display_name, props[:displayName]
      assert_equal user.resource.wood, props[:resources][:wood]
      assert_equal user.villager.unemployed, props[:villagers][:unemployed]
      assert_equal user.building.woodcutter_hut, props[:buildings][:woodcutterHut]
    end
  end

  test 'show XHR returns a valid JSON schema' do
    user = User.first
    sign_in user
    get root_url, xhr: true

    expected_response = {
      'id' => user.id,
      'displayName' => user.display_name,
      'gender' => user.gender,
      'avatar' => user.avatar,
      'job' => user.job,
      'resources' => {
        'wood' => user.resource.wood,
        'stone' => user.resource.stone,
        'gold' => user.resource.gold,
        'food' => user.resource.food
      },
      'villagers' => {
        'unemployed' => user.villager.unemployed,
        'trainee' => user.villager.trainee,
        'woodcutter' => user.villager.woodcutter,
        'stonecutter' => user.villager.stonecutter,
        'miner' => user.villager.miner,
        'farmer' => user.villager.farmer,
        'builder' => user.villager.builder
      },
      'buildings' => {
        'woodcutterHut' => user.building.woodcutter_hut,
        'quarry' => user.building.quarry,
        'mine' => user.building.mine,
        'farm' => user.building.farm,
        'toolshed' => user.building.toolshed
      }
    }

    assert_equal expected_response, JSON.parse(@response.body)
  end
end
