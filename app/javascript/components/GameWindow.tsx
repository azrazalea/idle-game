import * as React from 'react'
import _ from 'lodash'
import Table from 'react-bootstrap/Table'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Form from 'react-bootstrap/Form'
import { WindowInterface } from '../interfaces/game_interfaces'

export default class GameWindowComponent extends React.Component<WindowInterface, {editingDisplayName: boolean, editingGender: boolean}> {
  constructor (props: WindowInterface) {
    super(props)
    this.state = { editingDisplayName: false, editingGender: false }
  }

  toggleGenderState = () => {
    this.setState(_.merge({}, this.state, { editingGender: !this.state.editingGender }))
  }

  toggleDisplayNameState = () => {
    this.setState(_.merge({}, this.state, { editingDisplayName: !this.state.editingDisplayName }))
  }

  render () {
    const { gameState, updateUser } = this.props
    const { resources, villagers, buildings } = gameState

    const job = (gameState.job != null ? gameState.job : 'Nothing')
    const options = [
      { value: 'nothing', label: 'Nothing' },
      { value: 'builder', label: 'Building' },
      { value: 'farmer', label: 'Farming' },
      { value: 'miner', label: 'Mining' },
      { value: 'recruiter', label: 'Recruiter' },
      { value: 'stonecutter', label: 'Stonecutting' },
      { value: 'trainer', label: 'Training' },
      { value: 'woodcutter', label: 'Woodcutting' }
    ]
    const curriedUpdate = _.curry(updateUser, 2)
    const wrap = (key: string) => { return (event: React.ChangeEvent<HTMLInputElement & HTMLSelectElement>) => { return curriedUpdate(key)(event.currentTarget.value) } }
    const updateJob = wrap('job')
    const updateGender = wrap('gender')
    const updateDisplayName = wrap('display_name')

    return (
      <div data-testid="game-window">
        {this.state.editingDisplayName
          ? <Form.Control onChange={updateDisplayName} onBlur={this.toggleDisplayNameState} defaultValue={gameState.displayName} />
          : <h4 id="greeting" onDoubleClick={this.toggleDisplayNameState}>Welcome, {gameState.displayName}!</h4>}
        {this.state.editingGender
          ? <Form.Control onChange={updateGender} onBlur={this.toggleGenderState} defaultValue={gameState.gender} />
          : <span id="gender" onDoubleClick={this.toggleGenderState}>Gender: {gameState.gender} </span>}
        <Form>
          <Form.Group as={Row} className="mb-3">
            <Form.Label column id="job" sm={2}>Current Job:</Form.Label>
            <Col sm={4}>
              <Form.Select defaultValue={job} size="sm" onChange={updateJob}>
                {options.map((option) => <option key={option.value} value={option.value}>{option.label}</option>)}
              </Form.Select>
            </Col>
          </Form.Group>
      </Form>

      <Table striped bordered>
        <thead>
          <tr>
          <th colSpan={2}>Resources</th>
          <th colSpan={2}>Villagers</th>
          <th colSpan={2}>Buildings</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Gold</td><td>{resources.gold}</td>
            <td>Miner</td><td>{villagers.miner}</td>
            <td>Mine</td><td>{buildings.mine}</td>
          </tr>
          <tr>
            <td>Food</td><td>{resources.food}</td>
            <td>Farmer</td><td>{villagers.farmer}</td>
            <td>Farm</td><td>{buildings.farm}</td>
          </tr>
          <tr>
            <td>Wood</td><td>{resources.wood}</td>
            <td>Woodcutter</td><td>{villagers.woodcutter}</td>
            <td>Woodcutter Hut</td><td>{buildings.woodcutterHut}</td>
          </tr>
          <tr>
            <td>Stone</td><td>{resources.stone}</td>
            <td>Stonecutter</td><td>{villagers.stonecutter}</td>
            <td>Quarry</td><td>{buildings.quarry}</td>
          </tr>
          <tr>
            <td></td><td></td>
            <td>Builder</td><td>{villagers.builder}</td>
            <td>Toolshed</td><td>{buildings.toolshed}</td>
          </tr>
          <tr>
            <td></td><td></td>
            <td>Unemployed</td><td>{villagers.unemployed}</td>
            <td></td><td></td>
          </tr>
          <tr>
            <td></td><td></td>
            <td>Trainee</td><td>{villagers.trainee}</td>
            <td></td><td></td>
          </tr>
        </tbody>
      </Table>
      </div>
    )
  }
};
