module.exports = {
  // The root of your source code, typically /src
  // `<rootDir>` is a token Jest substitutes
  roots: ['app/javascript/'],

  // Jest transformations -- this adds support for TypeScript
  // using ts-jest
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  },

  setupFilesAfterEnv: [
    '@testing-library/jest-dom/extend-expect',
    'jest-extended/all'
  ],

  testEnvironment: 'jsdom'
}
