# frozen_string_literal: true

class CreateResources < ActiveRecord::Migration[7.0]
  def change
    create_table :resources do |t|
      t.integer :gold, default: 0, null: false
      t.integer :wood, default: 0, null: false
      t.integer :stone, default: 0, null: false
      t.integer :food, default: 0, null: false
      t.references :user, null: false, foreign_key: true, index: { unique: true }

      t.timestamps
    end
  end
end
