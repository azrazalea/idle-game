# frozen_string_literal: true

class Building < ApplicationRecord
  belongs_to :user
  validates :user, uniqueness: true
end
