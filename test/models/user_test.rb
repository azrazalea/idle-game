# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'can create a user' do
    user = User.new
    user.email = 'hey@hey.com'
    user.password = 'woot2343'
    user.display_name = 'I am a little teapot'
    user.gender = 'Non-binary'
    user.avatar = 'meh'
    user.job = 'protesting'
    assert user.save!
    assert user.resource.persisted? # Verify resource was created as well
    assert user.villager.persisted?
    assert user.building.persisted?
  end
end
