# frozen_string_literal: true

class CreateUserJobArgument < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :job_argument, :string
  end
end
