// Entry point for the build script in your package.json
import { mountComponents } from 'react-rails-ujs'

import GameWindow from './components/GameWindow'
import Index from './components/Index'

import 'bootstrap'

mountComponents({
  GameWindow,
  Index
})
