export interface BuildingsInterface {
  woodcutterHut: number;
  quarry: number;
  mine: number;
  farm: number;
  toolshed: number;
};

export interface ResourcesInterface {
  wood: number;
  stone: number;
  gold: number;
  food: number;
};

export interface VillagersInterface {
  unemployed: number;
  trainee: number;
  woodcutter: number;
  stonecutter: number;
  miner: number;
  farmer: number;
  builder: number;
};

export interface GameInterface {
  id: number;
  displayName: string;
  gender: string;
  avatar: string | null;
  job: string | null;
  jobArgument: string | null;
  buildings: BuildingsInterface;
  resources: ResourcesInterface;
  villagers: VillagersInterface;
};

export interface WindowInterface {
  gameState: GameInterface;
  updateUser(key: string, value: string): void;
};
